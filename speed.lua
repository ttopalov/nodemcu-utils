-- https://gitlab.com/ttopalov/nodemcu-utils
-- Module for controlling a dual-motor chasis (e.g. tank chasis) using DC motors with nodeMCU's PWM
-- Tested with L293D motor driver module, but should work well with any other compatible driver
-- MOTORS.config contains the pin numbers the driver was connected to.
-- These should only be changed BEFORE MOTORS.init() is called
-- MOTORS.config.LOW_THRESHOLD should be set to the lowest applied power that actually moves the motors in order to avoid unnecessary load with no effect
-- MOTORS.setPower(left, right) sets the power to the motors in the range [-1023, 1023]

MOTORS = {}

MOTORS.config = {
    A1 = 4,
    A2 = 7,
    PWMA = 2,
    B1 = 3,
    B2 = 0,
    PWMB = 6,
    LOW_THRESHOLD = 500
}

function MOTORS.init()
    gpio.mode(MOTORS.config.A1, gpio.OUTPUT);
    gpio.mode(MOTORS.config.A2, gpio.OUTPUT);
    gpio.mode(MOTORS.config.B1, gpio.OUTPUT);
    gpio.mode(MOTORS.config.B2, gpio.OUTPUT);
    pwm.setup(MOTORS.config.PWMA, 100, 0);
    pwm.setup(MOTORS.config.PWMB, 100, 0);
end

local function adjustPower(p)
    if (p < 50) then
        return 0
    else
        local val = MOTORS.config.LOW_THRESHOLD + p * (1023 - MOTORS.config.LOW_THRESHOLD) / 1023
        val = math.max(0, val)
        return math.min(val, 1023)
    end
end

local function setDuty(speed, in1, in2, pwmPin)
    if (speed >= 0) then
        gpio.write(in1, 0);
        gpio.write(in2, 1);
        pwm.setduty(pwmPin, adjustPower(speed));
    else
        gpio.write(in1, 1);
        gpio.write(in2, 0);
        pwm.setduty(pwmPin, adjustPower(-speed));
    end
end

function MOTORS.setPower(left, right)
    setDuty(right, MOTORS.config.A1, MOTORS.config.A2, MOTORS.config.PWMA);
    setDuty(left, MOTORS.config.B1, MOTORS.config.B2, MOTORS.config.PWMB);
end
